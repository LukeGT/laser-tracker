#include <stdio.h>
#include <algorithm>
#include <opencv2/opencv.hpp>
#include <opencv2/nonfree/nonfree.hpp>
#include <iostream>
#include <fstream>

using namespace cv;
using namespace std;

// Globals
int frame_skip = 0;
int target_size = 300;
double target_outside = 3;
double steadiness = 0.9;
string video;
string csv_file = "output.csv";
bool debug;
Vec3i marker_weights(2, 1, 1);
Vec3i dot_weights(2, 1, 1);

// Measurements in mm
const int marker_distance = 1400; // Horizontal/Vertical distance between coloured markers
const int marker_size = 76; // The width/height of the marker itself
const int marker_height = 363; // The height of the centre of a bottom marker from the ground
const int robot_height = 360; // The height of the laser on the robot from the robot's feet when in the standing position
const int robot_distance = 3000; // The distance from the robot's feet to the centre of the base of the markers, along the ground
const double laser_angle = 3.141592653589793/2; // The angle between the robot's torso and the laser

const Point2d ideal_corners[4] = {
	Point2d(-1, -1), // Top left
	Point2d(1, -1), // Bottom left
	Point2d(1, 1), // Bottom right
	Point2d(-1, 1), // Top right
};

VideoCapture video_capture;

// Classes
class Region {
public:
	Point2d pos;
	int count;

	Region() :
		pos(0, 0),
		count(0)
	{}
};

class MouseCallbackData {
public:
	Vec3b* target_colour;
	Mat* frame;

	MouseCallbackData(Vec3b* _target_colour, Mat* _frame) {
		target_colour = _target_colour;
		frame = _frame;
	}
};

// Function protoypes
bool flood_fill(Mat_<int> flood, Mat_<int> target, int threshold, int row, int col, int id, int depth = 0);
bool read_args(int argc, char** argv);
void usage();
void display(const char* title, const Mat &image, int wait = 0);
void configureMarkerColour(Vec3b &target_colour, Mat &frame_orig, Mat &frame);
void onMouse(int event, int x, int y, int, void* data_pointer);
void configureTargetAmount(Mat &frame_orig, Mat_<int> &target, Vec3i &weights, int &target_amount, int &threshold);
void configureDistanceWeights(Mat &frame, Vec3b &target_colour, Vec3i &weights);
Mat_<int> calculateDistances(Mat &frame, Vec3b &target_colour, Vec3i &weights);
int max_distance(Vec3i &weights);
int findThreshold(int target_amount, Mat_<int> &target, Vec3i &weights);
template<class T> T median(vector<T> values);
Vec3b findMedianWithinThreshold(int threshold, Mat_<int> &target, Mat &frame);
vector<Region> findRegions(Mat_<int> &target, int threshold);
vector<Region> findKLargestRegions(vector<Region> regions, int k);
void orientCorners(vector<Region> &corners);
Mat getCornerTransform(vector<Point2d> corners);

// Main
int main(int argc, char** argv) {

	if (!read_args(argc, argv)) {
		return 1;
	}
	
	Vec3b dot_colour(0, 128, 255);
	Vec3b target_colour(0, 0, 0);
	int threshold = max_distance(marker_weights);
	int dot_threshold = 0;
	int target_amount = 0;
	int dot_target_amount = 0;
	Mat transform;
	
	vector<Point2d> stable_corners(4);
	vector<Point2d> stable_corners_std_dev(4);
	int num_incorporated = 0;
	bool updating_corners = true;
	
	bool playing = true;

	ofstream output_csv(csv_file, ios::out | ios::trunc);
	if (!output_csv.is_open()) {
		printf("Unable to open output csv file.\n");
		return 2;
	}

	output_csv << "frame,marker_x,marker_y\n";

	// Read video
	video_capture.open(video);

	int frame_num = 0;

	for (int a = 0; a < frame_skip; ++a) {
		Mat dummy_frame;
		video_capture >> dummy_frame;
		frame_num++;
		if (dummy_frame.empty()) break;
		printf("Skipping frames... %d\r", a);
	}

	while(true) {

		// Load frame
		Mat frame_orig;
		video_capture >> frame_orig;
		frame_num++;
		if (frame_orig.empty()) break;

		// Convert to HSV
		Mat frame;
		cvtColor(frame_orig, frame, CV_BGR2HSV);

		if (debug) printf("------------- Begin Frame -------------\n");

		if (updating_corners) {

			// Configure marker colour if not initialised
			if (target_colour == Vec3b(0, 0, 0)) {
				configureMarkerColour(target_colour, frame_orig, frame);
			}

			// Calculate distances to target colour
			Mat_<int> target = calculateDistances(frame, target_colour, marker_weights);
			if (debug) display("Distances", target * 256 * 256 / max_distance(marker_weights), -1);

			// Calculate threshold
			if (target_amount == 0) {
				// Configure a good target amount
				configureTargetAmount(frame_orig, target, marker_weights, target_amount, threshold);
			} else {
				// Find the threshold that yields the closest thing to the configured target amount
				threshold = findThreshold(target_amount, target, marker_weights);
			}
			if (debug) printf("Selected threshold: %d\n", threshold);

			// Find a new target colour via median
			target_colour = findMedianWithinThreshold(threshold, target, frame);
			if (debug) printf("(%d, %d, %d)\n", target_colour[0], target_colour[1], target_colour[2]);

			// Find the four largest contiguous regions of area within the threshold
			vector<Region> regions = findRegions(target, threshold);
			vector<Region> corners = findKLargestRegions(regions, 4);

			// Draw the corners onto the original image
			for (int a = 0; a < 4; ++a) {	
				line(frame_orig, corners[a].pos + Point2d(-2, 0), corners[a].pos + Point2d(2, 0), Scalar(255, 0, 0));
				line(frame_orig, corners[a].pos + Point2d(0, -2), corners[a].pos + Point2d(0, 2), Scalar(255, 0, 0));
			}
		
			// Correspond the corners with their ideal values
			orientCorners(corners);

			if (debug) for (int a = 0; a < 4; ++a) {
				printf("Corner %d: (%f, %f) [%d]\n", a, corners[a].pos.x, corners[a].pos.y, corners[a].count);
			}

			if (num_incorporated == 0) {
			
				for (int a = 0; a < 4; ++a) {
					stable_corners[a] = corners[a].pos;
				}
				num_incorporated++;

			} else if (num_incorporated == 1) {

				for (int a = 0; a < 4; ++a) {
					stable_corners[a] = (stable_corners[a] + corners[a].pos) * 0.5;
				}
				num_incorporated++;
				
				for (int a = 0; a < 4; ++a) {
					Point2d deviation = stable_corners[a] - corners[a].pos;
					stable_corners_std_dev[a].x = sqrt(deviation.x*deviation.x);
					stable_corners_std_dev[a].y = sqrt(deviation.y*deviation.y);
				}

			} else {

				vector<Point2d> deviation(4);
				for (int a = 0; a < 4; ++a) {
					deviation[a].x = abs(stable_corners[a].x - corners[a].pos.x);
					deviation[a].y = abs(stable_corners[a].y - corners[a].pos.y);
				}

				bool performed_update = false;
			
				// Incorporate new information into stable corners, if it's within reason
				for (int a = 0; a < 4; ++a) {
					if (deviation[a].x < stable_corners_std_dev[a].x * 5
						&& deviation[a].y < stable_corners_std_dev[a].y * 5
						&& stable_corners_std_dev[a].x > 0.2
						&& stable_corners_std_dev[a].y > 0.2) {

						stable_corners[a].x = stable_corners[a].x * steadiness + corners[a].pos.x * (1 - steadiness);
						stable_corners[a].y = stable_corners[a].y * steadiness + corners[a].pos.y * (1 - steadiness);

						stable_corners_std_dev[a].x = sqrt(stable_corners_std_dev[a].x * stable_corners_std_dev[a].x * steadiness + deviation[a].x * deviation[a].x * (1- steadiness));
						stable_corners_std_dev[a].y = sqrt(stable_corners_std_dev[a].y * stable_corners_std_dev[a].y * steadiness + deviation[a].y * deviation[a].y * (1- steadiness));
					
						performed_update = true;
					}
				}

				if (!performed_update) {
					updating_corners = false;
					printf("Corners stabilised\n");
				}
			
				num_incorporated++;
			}
		
			// Calculate the perspective transform to go from image co-ordinates to the marker plane in real-world
			transform = getCornerTransform(stable_corners);
		}

		// Warp the image flat
		Mat flat;
		int size = (int)(target_size * target_outside * 2);
		warpPerspective(frame_orig, flat, transform, Size(size, size));

		Mat flat_hsv;
		cvtColor(flat, flat_hsv, CV_BGR2HSV);

		//Mat flat_hsv_blur;
		//GaussianBlur(flat_hsv, flat_hsv, Size(3, 3), 0, 0);

		Mat_<int> dot_target = calculateDistances(flat_hsv, dot_colour, dot_weights);
		display("Red distances", dot_target * 255 * 255 / max_distance(dot_weights), -1);
		
		if (dot_target_amount == 0) {
			
			configureTargetAmount(flat, dot_target, dot_weights, dot_target_amount, dot_threshold);
			dot_colour = findMedianWithinThreshold(dot_threshold, dot_target, flat_hsv);
			dot_target = calculateDistances(flat_hsv, dot_colour, dot_weights);
			configureTargetAmount(flat, dot_target, dot_weights, dot_target_amount, dot_threshold);

			printf("Target amount: %d\n", dot_target_amount);
			printf("(%d, %d, %d)\n", dot_colour[0], dot_colour[1], dot_colour[2]);
		} else {
			//dot_threshold = findThreshold(dot_target_amount, dot_target, dot_weights);
		}
		
		Mat_<uchar> mask(dot_target.size(), 0);
		for (int a = 0; a < dot_target.rows; ++a) {
			for (int b = 0; b < dot_target.cols; ++b) {
				if (dot_target[a][b] < dot_threshold) {
					mask[a][b] = 255;
				}
			}
		}
		display("Dot", mask, -1);

		//Erode and dilate
		Mat element = getStructuringElement(MORPH_CROSS, Size(3, 3), Point(1, 1));
		dilate(mask, mask, element);
		erode(mask, mask, element);
		erode(mask, mask, element);
		dilate(mask, mask, element);
		display("Erode/Dilate", mask, -1);

		vector<int> x_coords;
		vector<int> y_coords;
		int count = 0;
		for (int a = 0; a < mask.rows; ++a) {
			for (int b = 0; b < mask.cols; ++b) {
				if (mask[a][b]) {
					count++;
					x_coords.push_back(b);
					y_coords.push_back(a);
				}
			}
		}
		printf("Dot pixels: %d\n", count);

		if (count > 0) {

			Point2i dot_coords(median(x_coords), median(y_coords));

			printf("(%d, %d)\n", dot_coords.x - flat.cols/2, dot_coords.y - flat.rows/2);

			line(flat, dot_coords + Point2i(-10, 0), dot_coords + Point2i(-3, 0), Scalar(255, 255, 0));
			line(flat, dot_coords + Point2i(10, 0), dot_coords + Point2i(3, 0), Scalar(255, 255, 0));
			line(flat, dot_coords + Point2i(0, -10), dot_coords + Point2i(0, -3), Scalar(255, 255, 0));
			line(flat, dot_coords + Point2i(0, 10), dot_coords + Point2i(0, 3), Scalar(255, 255, 0));

			int x = dot_coords.x - flat.cols/2;
			int y = dot_coords.y - flat.rows/2;
			double real_x = (double)(x) / target_size * marker_distance;
			double real_y = -(double)(y) / target_size * marker_distance;
			double laser_height = real_y + marker_distance/2.0 + marker_height;
			double adjusted_robot_distance = sqrt( robot_distance * robot_distance + real_x * real_x );

			double theta = laser_angle - atan( (laser_height - robot_height) / adjusted_robot_distance );
			
			// Output to CSV
			output_csv << frame_num
				<< ',' << x
				<< ',' << y
				<< ',' << theta
				<< '\n';
		}

		
		display("Flat", flat, -1);

		imshow("Video", frame_orig);
		int key;
		if (playing) {
			key = waitKey(1000/50);
		} else {
			key = waitKey(0);
		}
		if (key == 32) {
			playing = !playing;
		} else if (key == 'c') {
			Mat flat_hsv_blur;
			GaussianBlur(flat_hsv, flat_hsv_blur, Size(5, 5), 0, 0);
			configureMarkerColour(dot_colour, flat, flat_hsv_blur);
			configureDistanceWeights(flat_hsv_blur, dot_colour, dot_weights);
		}
	}

	return 0;
}

void configureMarkerColour(Vec3b &target_colour, Mat &frame_orig, Mat &frame) {

	const char* title = "Click on a marker";

	namedWindow(title);
	imshow(title, frame_orig);

	MouseCallbackData data(&target_colour, &frame);
	setMouseCallback(title, onMouse, &data);

	while (waitKey(1000/60) != 13);

	destroyWindow(title);
}

void onMouse( int event, int x, int y, int, void* data_pointer) {

	if (event != EVENT_LBUTTONDOWN) return;

	MouseCallbackData data = *(MouseCallbackData*)data_pointer;
	*data.target_colour = data.frame->at<Vec3b>(y, x);

	printf("Selected colour: (%d, %d, %d)\n", (*data.target_colour)[0], (*data.target_colour)[1], (*data.target_colour)[2]);
}

void configureTargetAmount(Mat &frame_orig, Mat_<int> &target, Vec3i &weights, int &target_amount, int &threshold) {
	
	const char* title = "Slide until only the markers are visible";

	// Rough guess at a good threshold
	int min_distance = INT_MAX;
	for (int a = 0; a < target.rows; ++a) {
		for (int b = 0; b < target.cols; ++b) {
			min_distance = min(target[a][b], min_distance);
		}
	}
	threshold = min_distance * 2;

	namedWindow(title, WINDOW_NORMAL);
	imshow(title, frame_orig);
	createTrackbar("Threshold", title, &threshold, max_distance(weights));

	while (true) {

		target_amount = 0;
		Mat_<Vec3b> masked_frame(frame_orig.size(), Vec3b(255, 255, 0));

		for (int a = 0; a < masked_frame.rows; ++a) {
			for (int b = 0; b < masked_frame.cols; ++b) {
				if (target[a][b] < threshold) {
					target_amount++;
					masked_frame[a][b] = frame_orig.at<Vec3b>(a, b);
				}
			}
		}

		imshow(title, masked_frame);
		if (waitKey(1000/60) == 13) break;
	}

	destroyWindow(title);
}

void configureDistanceWeights(Mat &frame, Vec3b &target_colour, Vec3i &weights) {

	const char* title = "Fiddle with the sliders until the target area is most distinct from the rest of the image";

	namedWindow(title);
	imshow(title, calculateDistances(frame, target_colour, weights) * 255 * 255 / max_distance(weights));
	createTrackbar("Hue weight", title, &weights[0], 10);
	createTrackbar("Saturation weight", title, &weights[1], 10);
	createTrackbar("Value weight", title, &weights[2], 10);

	while (true) {
		
		imshow(title, calculateDistances(frame, target_colour, weights) * 255 * 255 / max_distance(weights));

		if (waitKey(1000/60) == 13) break;
	}

	destroyWindow(title);
}

Mat_<int> calculateDistances(Mat &frame, Vec3b &target_colour, Vec3i &weights) {
	
	Mat_<int> target(frame.size());
	
	for (int a = 0; a < target.rows; ++a) {
		for (int b = 0; b < target.cols; ++b) {
			int hue_dist = target_colour[0] - frame.at<Vec3b>(a, b)[0];
			hue_dist = min( min( abs(hue_dist), abs(hue_dist-180) ), abs(hue_dist+180) );
			int sat_dist = abs(target_colour[1] - frame.at<Vec3b>(a, b)[1]);
			int val_dist = abs(target_colour[2] - frame.at<Vec3b>(a, b)[2]);
			target[a][b] = hue_dist * weights[0] + sat_dist * weights[1] + val_dist * weights[2];
		}
	}

	return target;
}

int max_distance(Vec3i &weights) {
	return 180 * weights[0] + 255 * weights[1] + 255 * weights[2];
}

int findThreshold(int target_amount, Mat_<int> &target, Vec3i &weights) {

	int min_threshold = 0;
	int max_threshold = max_distance(weights);

	while (max_threshold - min_threshold > 1) {

		int mid = (min_threshold + max_threshold) / 2;
			
		int count = 0;
		for (int a = 0; a < target.rows; ++a) {
			for (int b = 0; b < target.cols; ++b) {
				if (target[a][b] < mid) {
					count++;
				}
			}
		}

		if (count > target_amount) {
			max_threshold = mid;
		} else {
			min_threshold = mid;
		}
	}

	return min_threshold;
}

template<class T>
T median(vector<T> values) {
	
	if (values.empty()) return -1;

	int middle = values.size()/2;
	nth_element(values.begin(), values.begin() + middle, values.end());
	values[middle] = values[middle];

	return values[middle];
}

Vec3b findMedianWithinThreshold(int threshold, Mat_<int> &target, Mat &frame) {
	
	vector<uchar> hues;
	vector<uchar> sats;
	vector<uchar> vals;

	for (int a = 0; a < target.rows; ++a) {
		for (int b = 0; b < target.cols; ++b) {
			if (target[a][b] < threshold) {
				hues.push_back(frame.at<Vec3b>(a, b)[0]);
				sats.push_back(frame.at<Vec3b>(a, b)[1]);
				vals.push_back(frame.at<Vec3b>(a, b)[2]);
			}
		}
	}

	return Vec3b(median(hues), median(sats), median(vals));
}

vector<Region> findRegions(Mat_<int> &target, int threshold) {
	
	// Find contiguous regions
	Mat_<int> flood(target.size(), 0);
	int id = 0;

	for (int a = 0; a < target.rows; ++a) {
		for (int b = 0; b < target.cols; ++b) {
			if (target[a][b] < threshold && flood[a][b] == 0) {
				flood_fill(flood, target, threshold, a, b, ++id);
			}
		}
	}

	if (debug) {
		printf("regions: %d\n", id);
		display("Flood", flood * 256 * 256, -1);
	}

	// Summarise regions
	vector<Region> regions(id);

	for (int a = 0; a < flood.rows; ++a) {
		for (int b = 0; b < flood.cols; ++b) {
			if (flood[a][b] > 0) {
				regions[flood[a][b]-1].count++;
				regions[flood[a][b]-1].pos.y += a;
				regions[flood[a][b]-1].pos.x += b;
			}
		}
	}

	for (unsigned int a = 0; a < regions.size(); ++a) {
		regions[a].pos.x /= regions[a].count;
		regions[a].pos.y /= regions[a].count;
	}

	return regions;
}

bool flood_fill(Mat_<int> flood,  Mat_<int> target, int threshold, int row, int col, int id, int depth) {

	if (depth > flood.rows + flood.cols // Search too deep
		|| row < 0 // Outside of bounds
		|| col < 0
		|| row >= flood.rows
		|| col >= flood.cols
		|| target[row][col] >= threshold // Not part of a target marker
		|| flood[row][col] == id // Previously visited
	) {
		return false;
	}

	flood[row][col] = id;

	flood_fill(flood, target, threshold, row+1, col, id, depth + 1);
	flood_fill(flood, target, threshold, row, col+1, id, depth + 1);
	flood_fill(flood, target, threshold, row-1, col, id, depth + 1);
	flood_fill(flood, target, threshold, row, col-1, id, depth + 1);

	return true;
}

vector<Region> findKLargestRegions(vector<Region> regions, int k) {

	vector<Region> largest(k);

	for (unsigned int a = 0; a < regions.size(); ++a) {
		if (regions[a].count > largest[0].count) {
			largest[0] = regions[a];
			for (int b = 0; b < k-1 && largest[b].count > largest[b+1].count; ++b) {
				swap(largest[b], largest[b+1]);
			}
		}
	}
	return largest;
}

void orientCorners(vector<Region> &corners) {
	
	// Find rough centre
	Point2d centre(0, 0);
	for (int a = 0; a < 4; ++a) {
		centre += corners[a].pos;
	}
	centre *= 1.0/4.0;

	Point2d relative_corners[4];
	for (int a = 0; a < 4; ++a) {
		relative_corners[a] = corners[a].pos - centre;
	}

	// Assign each marker to it's most likely corner
	for (int a = 0; a < 4; ++a) {

		double max_cos = -1;
		int max_index = -1;

		for (int b = a; b < 4; ++b) {
			double cos = relative_corners[b].dot(ideal_corners[a]) / ( sqrt( relative_corners[b].dot(relative_corners[b]) * ideal_corners[a].dot(ideal_corners[a]) ) );
			if (cos > max_cos) {
				max_cos = cos;
				max_index = b;
			}
		}
		swap(relative_corners[a], relative_corners[max_index]);
		swap(corners[a], corners[max_index]);
	}
}

Mat getCornerTransform(vector<Point2d> corners) {

	Point2f image_coords[4];
	Point2f dest_coords[4];

	for (int a = 0; a < 4; ++a) {
		image_coords[a] = corners[a];
		dest_coords[a] = (ideal_corners[a] + Point2d(target_outside, target_outside)) * target_size;
	}
		
	return getPerspectiveTransform(image_coords, dest_coords);
}

bool read_args(int argc, char** argv) {
	
	if (argc <= 1) {
		usage();
		return false;
	}

	for (int index = 1; index < argc; ++index) {

		if (argv[index][0] != '-') {
			usage();
			return false;
		}

		switch (argv[index][1]) {

		case 'v':
			video = argv[++index];
			break;

		case 's':
			target_size = stoi(argv[++index]);
			break;

		case 'o':
			target_outside = stof(argv[++index]);
			break;
		
		case 'd':
			debug = true;
			break;

		case 'w':
			steadiness = stof(argv[++index]);
			break;
			
		case 'h':
			usage();
			break;

		case 'j':
			frame_skip = stoi(argv[++index]);
			break;

		case 'c':
			csv_file = argv[++index];
			break;

		default:
			usage();
			return false;
		}
	}

	if (debug) {
		printf("size: %d\nvideo: %s\n\n", target_size, video.c_str());
	}

	return true;
}

void usage() {
	printf("Usage:\n"
		"\t-v video\n"
		"\t-s size of target\n"
		"\t-o scale of target\n"
		"\t-d (enable debug logging)\n"
		"\t-w steadiness of marker tracking\n"
		"\t-h (display help)\n"
		"\t-j jump to nth frame\n"
		"\t-c set the output csv filename\n"
	);
}

void display(const char* title, const Mat &image, int wait) {
	namedWindow(title, WINDOW_NORMAL);
	imshow(title, image);
	if (wait > 0) {
		waitKey(wait);
	}
}